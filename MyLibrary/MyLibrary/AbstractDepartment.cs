﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLibrary
{
    abstract class AbstractDepartment: IDepartment
    {
        public string Name { get; set; }
        public List<Book> ListOfBooks { get; }

        protected AbstractDepartment(string name)
        {
            Name = name;
        }

    }
}
