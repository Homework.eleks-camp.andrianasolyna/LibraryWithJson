﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLibrary
{
    static class JSONSerializer
    {
        public static void LibraryToJSON(Library library, string fileName)
        {
            File.WriteAllText(@fileName, JsonConvert.SerializeObject(library));
        }

        public static Library LibraryFromJSON(string fileName)
        {
            using (StreamReader file = File.OpenText(@fileName))
            {
                JsonSerializer serializer = new JsonSerializer();
                return (Library)serializer.Deserialize(file, typeof(Library));
            }
        }
    }
}
