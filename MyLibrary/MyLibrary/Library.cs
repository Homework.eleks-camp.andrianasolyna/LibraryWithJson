﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLibrary
{
    class Library
    {
        private List<Department> _listOfDepartments;

        public string Name { get; set; }

        public List<Department> ListOfDepartments
        {
            get
            {
                if (_listOfDepartments != null)
                {
                    return _listOfDepartments;
                }
                _listOfDepartments = new List<Department>();
                return _listOfDepartments;
            }
        }

        public Library(string name)
        {
            Name = name;
        }

        public void AddDepartment(Department department)
        {
           ListOfDepartments.Add(department);
        }

        public override string ToString()
        {
            return $"Name {Name},";
        }

        public int GetNumberOfBooks()
        {
            int numberOfBooks = 0;
            foreach (var item in ListOfDepartments)
            {
                numberOfBooks += item.GetNumberOfBooks();
            }
            return numberOfBooks;
        }
    }
}
