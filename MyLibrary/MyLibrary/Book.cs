﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLibrary
{
    class Book: IComparable<Book>
    {
        private readonly Author _author;

        public Author BookAuthor
        {
            get
            {
                return _author;
            }
        }
        public uint NumberOfPages
        {
            get;
            set;  
        }

        public string Title
        {
            get;
            set;    
        }

        public Book()
        {
            _author = new Author();
            NumberOfPages = 0;
            Title = "";
            
        }

        public Book(Author author, uint numberOfPages, string title)
        {
            _author = author;
            ++_author.NumberOfBooks;
            NumberOfPages = numberOfPages;
            Title = title;
        }

        public int CompareTo(Book other)
        {
            return this.NumberOfPages.CompareTo(other.NumberOfPages);
        }

        public override string ToString()
        {
            return $"Title: {Title} Author: {_author} Number of pages: {NumberOfPages} \n";
        }
    }
}
