﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace MyLibrary
{
    static class XMLToLibrary
    {
        private static XElement AuthorToXML(Author author)
        {
            XElement authorXML = new XElement("author", new XAttribute("first_name",author.FirstName), new XAttribute("last_name", author.LastName), new XAttribute("number_of_books",author.NumberOfBooks));
            return authorXML;
        }

        private static XElement BookToXML(Book book)
        {
            XElement bookXML = new XElement("book", new XAttribute("title", book.Title), new XAttribute("pages", book.NumberOfPages), new XElement(AuthorToXML(book.BookAuthor)));
            return bookXML;
        }
        private static XElement DepartmentToXML(Department department)
        {
            XElement departmentXML = new XElement("department");
            departmentXML.Add(new XAttribute("name", department.Name));
            XElement booksInDepartment =new XElement("books");
            foreach (var book in department.ListOfBooks)
            {
                booksInDepartment.Add(BookToXML(book));
            }
            departmentXML.Add(booksInDepartment);
            return departmentXML;
        }

        public static void SaveToXML(Library library, string fileName)
        {
            XDocument doc = new XDocument();
            XElement lib = new XElement("library");
            lib.Add(new XAttribute("name", library.Name));
            XElement departmentInLibrary =new XElement("departments");
            foreach (var item  in library.ListOfDepartments)
            {
                departmentInLibrary.Add(DepartmentToXML(item));
            }
            lib.Add(departmentInLibrary);
            doc.Add(lib);
            doc.Save(fileName);
        }

        private static Author AuthorFromXML(XElement author)
        {
            return new Author(author.Attribute("first_name").Value, author.Attribute("last_name").Value);
        }

        private static Book BookFromXMl(XElement book)
        {
            return new Book(AuthorFromXML( book.Element("author")), UInt32.Parse(book.Attribute("pages").Value), book.Attribute("title").Value );
        }

        private static Department DepartmentFromXML(XElement department)
        {
            var departmentFromXML = new Department(department.Attribute("name").Value);
            foreach (var book in department.Element("books").Elements("book"))
            {
                departmentFromXML.AddBook(BookFromXMl(book));
            }
            return departmentFromXML;
        }


        public static Library ReadFromXML(string fileName)
        {
            XDocument doc = XDocument.Load(fileName);
            var libraryFromXml =new Library(doc.Root.Attribute("name").Value);
            foreach (var department in doc.Root.Element("departments").Elements("department"))
            {
                libraryFromXml.AddDepartment(DepartmentFromXML(department));
            }
            return libraryFromXml;
        }
    }
}
