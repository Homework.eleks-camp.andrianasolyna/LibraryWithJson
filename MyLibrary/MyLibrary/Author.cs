﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLibrary
{
    class Author : IComparable<Author>
    {
        public string FirstName
        {
            get;
        }

        public string LastName
        {
            get;
        }
        public uint NumberOfBooks
        {
            get;
            set;
        }

        public Author()
        {
            FirstName = "";
            LastName = "";
            NumberOfBooks = 0;
        }

        public Author(string firstName, string lastNmae) :this()
        {
            FirstName = firstName;
            LastName = lastNmae;
        }

        public override string ToString()
        {
            return $"Name: {FirstName} {LastName} NumberOfBooks:{NumberOfBooks} \n";
        }

        public int CompareTo(Author other)
        {
            return this.NumberOfBooks.CompareTo(other.NumberOfBooks);
        }
    }
}
