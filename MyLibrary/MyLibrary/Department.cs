﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLibrary
{
    class Department: AbstractDepartment, IComparable<IDepartment>
    {
        private List<Book> _listOfBooks = new List<Book>();

        public List<Book> ListOfBooks
        {
            get { return _listOfBooks; }
        }
        public Department(string name) : base(name)
        {
        }
        
        public void AddBook(Book book)
        {
            ListOfBooks.Add(book);
        }

        public int CompareTo(IDepartment other)
        {
            return this.ListOfBooks.Count.CompareTo(other.ListOfBooks.Count);
        }

        public override string ToString()
        {
            return $"Name {Name}, NumberOfBooks {ListOfBooks.Count} \n";
        }

        public int GetNumberOfBooks()
        {
            return ListOfBooks.Count;
        }
    }
}
