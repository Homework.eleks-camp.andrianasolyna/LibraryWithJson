﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyLibrary
{
    class Program
    {
        static void Main(string[] args)
        {
            Author author1 = new Author("Den", "Brown");
            Book book1 = new Book(author1, 400, "Angels&Demons");
            Book book2 = new Book(author1, 600, "SomeTitle");
            Department department1 = new Department("Brown");
            department1.AddBook(book1);
            department1.AddBook(book2);
            Library library = new Library("FirstLabrary");
            library.AddDepartment(department1);

            //using Linq to Xml
            XMLToLibrary.SaveToXML(library, "firstLibrary.xml");
            Library newLibrary = XMLToLibrary.ReadFromXML("firstLibrary.xml");

            //using Newtonsoft json serializer
            JSONSerializer.LibraryToJSON(library,"myLibrary.json");
            Library library2 = JSONSerializer.LibraryFromJSON("myLibrary.json");
            Console.WriteLine(library);
            Console.ReadKey();
        }
    }
}